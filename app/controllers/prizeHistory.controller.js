// Import PrizeHistorySchema
const prizeHistoryModel = require("../models/prizeHistory.model")
const userModel = require("../models/user.model")

const mongoose = require("mongoose")
//
const createPrizeHistory = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { user, prize } = req.body
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    let newPrizeHistory = {
        _id: new mongoose.Types.ObjectId(),
        user,
        prize
    }
    try {
        const result = await prizeHistoryModel.create(newPrizeHistory);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm get all
const getAllPrizeHistory = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await prizeHistoryModel.find();
    return res.status(200).json({
        result
    });
}
// Hàm get bằng Id
const getPrizeHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var prizeHistoryId = req.params.prizeHistoryId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "PrizeHistory Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await prizeHistoryModel.findById(prizeHistoryId);
    return res.status(200).json({
        result
    });
}
// Hàm get tất cả history
const getAllPrizeHistoryOfUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const username = req.query.username
    const condition = {}
    condition.username = username
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const findUser = await userModel.findOne(condition)
    if (findUser === null) {
        result = []
        return res.status(404).json({
            result: result
        })
    }
    else {
        const condition = {}
        condition.user = findUser._id
        const result = await prizeHistoryModel.find(condition);
        return res.status(200).json({
            result
        });
    }
}
const updatePrizeHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var prizeHistoryId = req.params.prizeHistoryId
    const { user, prize } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "PrizeHistoryId is invalid"
        })
    }

    // B3 Xử lý 
    let updated = {
        user,
        prize,
    }
    const result = await prizeHistoryModel.findByIdAndUpdate(PrizeHistoryId, updated);
    return res.status(200).json({
        status: "Update PrizeHistory successfully",
        result
    });
}
// Hàm xóa  bằng Id
const deletePrizeHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var prizeHistoryId = req.params.prizeHistoryId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await prizeHistoryModel.findByIdAndDelete(prizeHistoryId);
    return res.status(204).json({
        status: "Delete PrizeHistory successfully"
    });
}
//export
module.exports = {
    createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById, getAllPrizeHistoryOfUser
}

// Import userSchema
const userModel = require("../models/user.model")
const mongoose = require("mongoose")
//
const createUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { username, firstname, lastname } = req.body
    // B2 Kiểm tra dữ liệu
    if (!username) {
        return res.status(400).json({
            status: "Bad request",
            message: "username is required"
        })
    }
    if (!firstname) {
        return res.status(400).json({
            status: "Bad request",
            message: "firstname is required"
        })
    }
    if (!lastname) {
        return res.status(400).json({
            status: "Bad request",
            message: "lastname is required"
        })
    }
    // B3 Xử lý 
    let newUser = {
        _id: new mongoose.Types.ObjectId(),
        username,
        firstname,
        lastname
    }
    try {
        const result = await userModel.create(newUser);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm get all
const getAllUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.find();
    return res.status(200).json({
        result
    });
}
// Hàm get bằng Id
const getUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await userModel.findById(userId);
    return res.status(200).json({
        result
    });
}
const updateUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    const { username, firstname, lastname } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid"
        })
    }

    // B3 Xử lý 
    let updated = {
        username,
        firstname,
        lastname
    }
    const result = await userModel.findByIdAndUpdate(userId, updated);
    return res.status(200).json({
        status: "Update user successfully",
        result
    });
}
// Hàm xóa  bằng Id
const deleteUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.findByIdAndDelete(userId);
    return res.status(204).json({
        status: "Delete user successfully"
    });
}
//export
module.exports = {
    createUser, getAllUser, getUserById, updateUserById, deleteUserById
}

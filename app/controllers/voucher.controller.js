//Import Model
const voucherModel = require("../models/voucher.model")
const mongoose = require("mongoose")
// Hàm tạo 
const createVoucher = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { code, note, discount } = req.body
    // B2 Kiểm tra dữ liệu
    if (!code) {
        return res.status(400).json({
            status: "Bad request",
            message: "code is required"
        })
    }
    if (!discount) {
        return res.status(400).json({
            status: "Bad request",
            message: "discount is required"
        })
    }
    // B3 Xử lý 
    let newObj = {
        code,
        note,
        discount
    }
    try {
        const result = await voucherModel.create(newObj);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm get all
const getAllVoucher = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await voucherModel.find();
    return res.status(200).json({
        result
    });
}
// Hàm get bằng Id
const getVoucherById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherId = req.params.voucherId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await voucherModel.findById(voucherId);
    return res.status(200).json({
        result
    });
}
const updateVoucherById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherId = req.params.voucherId
    const { code, note, discount } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "VoucherId is invalid"
        })
    }

    // B3 Xử lý 
    let updated = {
        note,
        code,
        discount
    }
    const result = await voucherModel.findByIdAndUpdate(voucherId, updated);
    return res.status(200).json({
        status: "Update Voucher successfully",
        result
    });
}
// Hàm xóa  bằng Id
const deleteVoucherById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherId = req.params.voucherId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await voucherModel.findByIdAndDelete(voucherId);
    return res.status(204).json({
        status: "Delete Voucher successfully"
    });
}
// Export
module.exports = {
    createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById
}
// Import voucherHistorySchema
const voucherHistoryModel = require("../models/voucherHistory.model")
const userModel = require("../models/user.model")

const mongoose = require("mongoose")
//
const createVoucherHistory = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { user, voucher } = req.body
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    let newvoucherHistory = {
        _id: new mongoose.Types.ObjectId(),
        user,
        voucher
    }
    try {
        const result = await voucherHistoryModel.create(newvoucherHistory);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm get all
const getAllVoucherHistory = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await voucherHistoryModel.find();
    return res.status(200).json({
        result
    });
}
// Hàm get bằng Id
const getVoucherHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherHistoryId = req.params.voucherHistoryId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherHistory Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await voucherHistoryModel.findById(voucherHistoryId);
    return res.status(200).json({
        result
    });
}
const getAllVoucherHistoryOfUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const username = req.query.username
    const condition = {}
    condition.username = username
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const findUser = await userModel.findOne(condition)
    if (findUser === null) {
        result = []
        return res.status(404).json({
            result: result
        })
    }
    else {
        const condition = {}
        condition.user = findUser._id
        const result = await voucherHistoryModel.find(condition);
        return res.status(200).json({
            result
        });
    }
}
const updateVoucherHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherHistoryId = req.params.voucherHistoryId
    const { user, voucher } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherHistoryId is invalid"
        })
    }

    // B3 Xử lý 
    let updated = {
        user,
        voucher,
    }
    const result = await voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, updated);
    return res.status(200).json({
        status: "Update voucherHistory successfully",
        result
    });
}
// Hàm xóa  bằng Id
const deleteVoucherHistoryById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var voucherHistoryId = req.params.voucherHistoryId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await voucherHistoryModel.findByIdAndDelete(voucherHistoryId);
    return res.status(204).json({
        status: "Delete voucherHistory successfully"
    });
}
//export
module.exports = {
    createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById, getAllVoucherHistoryOfUser
}

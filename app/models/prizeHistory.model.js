//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const prizeHistorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    prize: {
        type: Schema.Types.ObjectId, ref: 'Prize',
        required: true
    }
}, {
    timestamps: true
}
)
// Biên dịch Schema
module.exports = mongoose.model("PrizeHistories", prizeHistorySchema)
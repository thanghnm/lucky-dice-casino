//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    firstname: {
        required: true,
        type: String,

    },
    lastname: {
        required: true,
        type: String,
    }
}, {
    timestamps: true
}
)
// Biên dịch Schema
module.exports = mongoose.model("User", userSchema)
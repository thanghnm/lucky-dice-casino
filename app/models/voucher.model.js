//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const voucherSchema = new Schema({
    code: {
        type: String,
        unique: true,
        required: true
    },
    discount: {
        required: true,
        type: Number,
    },
    note: {
        type: String,
    }
}, {
    timestamps: true
}
)
// Biên dịch Schema
module.exports = mongoose.model("Voucher", voucherSchema)
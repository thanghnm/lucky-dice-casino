//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const voucherHistorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    voucher: {
        type: Schema.Types.ObjectId, ref: 'Voucher',
        required: true
    }
}, {
    timestamps: true
}
)
// Biên dịch Schema
module.exports = mongoose.model("VoucherHistories", voucherHistorySchema)
const express = require("express");
const { createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteHistoryById, getAllDiceHistoryOfUser, playDice } = require("../controllers/diceHistory.controller");
const router = express.Router();
// Router tạo 
router.post("/", createDiceHistory)
router.post("/dice/", playDice)
// Router Get all
router.get("/", getAllDiceHistory)
router.get("/user/", getAllDiceHistoryOfUser)
// Router Get by ID
router.get("/:historyId", getDiceHistoryById)
// Router Update by ID
router.put("/:historyId", updateDiceHistoryById)
// Router Delete by Id
router.delete("/:historyId", deleteHistoryById)
//
module.exports = router
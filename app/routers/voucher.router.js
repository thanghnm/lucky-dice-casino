const express = require("express");
const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require("../controllers/voucher.controller");
const router = express.Router();

// Router Tạo
router.post("/", createVoucher)
// Router Get all
router.get("/", getAllVoucher)
// Router Get by Id
router.get("/:voucherId", getVoucherById)
// Router Update by Id
router.put("/:voucherId", updateVoucherById)
// Router Delete by Id
router.delete("/:voucherId", deleteVoucherById)

module.exports = router
